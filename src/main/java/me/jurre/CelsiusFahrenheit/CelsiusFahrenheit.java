package me.jurre.CelsiusFahrenheit;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

public class CelsiusFahrenheit extends HttpServlet {

    @Override
    public void init() {
    }

    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
        response.setContentType("text/html");

        // Hello
        PrintWriter out = response.getWriter();
        String docType =
            "<!DOCTYPE HTML>\n";
        String title = "Celsius Fahrenheit calculator";
        out.println(docType +
            "<HTML>\n" +
            "<HEAD><TITLE>" + title + "</TITLE>" +
            "<LINK REL=STYLESHEET HREF=\"styles.css\">" +
            "</HEAD>\n" +
            "<BODY BGCOLOR=\"#FDF5E6\">\n" +
            "<H1>" + title + "</H1>\n" +
            "  <P>Fahrenheit: " +
            getFahrenheit(request) +
            "</BODY></HTML>");
    }

    private String getFahrenheit(HttpServletRequest request) {
        try {
            return "" + (Double.parseDouble(request.getParameter("celsius")) * 9 / 5 + 32);
        } catch (NumberFormatException e) {
            return "input must be a number!";
        }
    }

    @Override
    public void destroy() {
    }
}