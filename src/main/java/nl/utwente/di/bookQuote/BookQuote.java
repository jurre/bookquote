package nl.utwente.di.bookQuote;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * Example of a Servlet that gets an ISBN number and returns the book price
 */
public class BookQuote extends HttpServlet {

    private static final long serialVersionUID = 1L;
    private Quoter quoter;

    @Override
    public void init() {
        quoter = new Quoter();
    }

    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
        response.setContentType("text/html");
        PrintWriter out = response.getWriter();

        String docType =
            "<!DOCTYPE HTML>\n";
        String title = "Book Quote";
        out.println(docType +
            "<HTML>\n" +
            "<HEAD><TITLE>" + title + "</TITLE>" +
            "<LINK REL=STYLESHEET HREF=\"styles.css\">" +
            "</HEAD>\n" +
            "<BODY BGCOLOR=\"#FDF5E6\">\n" +
            "<H1>" + title + "</H1>\n" +
            "  <P>ISBN number: " +
            request.getParameter("isbn") + "\n" +
            "  <P>Price: " +
            quoter.getBookPrice(request.getParameter("isbn")) +
            "</BODY></HTML>");
    }
}