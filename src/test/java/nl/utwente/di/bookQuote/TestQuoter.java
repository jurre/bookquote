package nl.utwente.di.bookQuote;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class TestQuoter {
    @Test
    public void testBook1() {
        Quoter quoter = new Quoter();
        double price = quoter.getBookPrice("1");
        assertEquals("Price of book 1", 10.0, price, 0.0);
    }
}